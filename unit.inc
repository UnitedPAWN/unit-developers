






#if defined __unit__included__
	#endinput
#else
	#define __unit__included__
#endif

#include <a_samp>
#tryinclude <YSI_Data\y_iterate>

#define INVALID_DB_ID DB2:-1
#define INVALID_RESULT_ID DB2Aresult:-1
#define INVALID_PREPARED_ID	DB2Prepared:-1
#define INVALID_GROUP_ID Group:-1
#define INVALID_COMMAND_ID Command:-1
#define INVALID_PERMISSION_ID Permission:-1
#define INVALID_PERM_ID Permission:-1
#define INVALID_INV_ID Inventory:-1
#define INVALID_SCRIPT_ID Script:-1

#define DB2:: DB2_

enum Error (+=-1) {
	ERROR_NONE = 1,
	ERROR_UNKNOWN,
	ERROR_INVALID_DB,
	ERROR_INVALID_NAME,
	ERROR_INVALID_NAME_LENGTH,
	ERROR_DB_REGISTERED,
	ERROR_FULL_DB_LIST
}

stock bool:operator!(Error: error)        return (error != ERROR_NONE);

enum e_COMMAND_ERRORS
{
	// The majority of these are even - odd numbers return "1" not "0".
	COMMAND_ZERO_RET      = 0 , // The command returned 0.
	COMMAND_OK            = 1 , // Called corectly.
	COMMAND_UNDEFINED     = 2 , // Command doesn't exist.
	COMMAND_DENIED        = 3 , // Can't use the command.
	COMMAND_HIDDEN        = 4 , // Can't use the command don't let them know it exists.
	COMMAND_NO_PLAYER     = 6 , // Used by a player who shouldn't exist.
	COMMAND_ALL_DISABLED  = 7 , // All commands are disabled for this player.
	COMMAND_BAD_PREFIX    = 8 , // Used "/" instead of "#", or something similar.
	COMMAND_DISABLED      = 9 , // This command is disabled
	COMMAND_INVALID_INPUT = 10  // Didn't type "/something".
}

enum {
	COMMAND_HIDE_ITER		= 1,
	COMMAND_HIDE_ITER2		= 2,
	COMMAND_HIDE_UHELP		= 4,
	COMMAND_HIDE_UHELP2		= 8,
	COMMAND_HIDE_CMD		= 16,
	COMMAND_HIDE_CHANGE		= 32,
	COMMAND_HIDE_PROTECTED	= 64,
	COMMAND_HIDE_PARAMS     = 128
}

enum Modifier {
	E_Mod_GodMode,
	E_Mod_NoWarp,
	E_Mod_NoCarSpawn,
	E_Mod_NoWeapon,
	E_Mod_OldUnused1,
	E_Mod_NoAnnounce,
	E_Mod_NoHeal,
	E_Mod_NoInfoChat,
	E_Mod_NoWeaponBuy,
	E_Mod_NoSpecial,
	E_Mod_NoJetpack,
	E_Mod_NoSuicide,
	E_Mod_NoCommands,
	E_Mod_NoChat,
	E_Mod_NoAnim
}

enum UnitCallback {
	CB_OnDialogResponse,
	CB_OnPlayerTryWarp,
	CB_OnPlayerWarp,
	CB_OnPlayerTrySpawnVehicle,
	CB_OnPlayerCommandReceived,
	CB_OnPlayerCommandRecognized,
	CB_OnPlayerCommandPerformed,
	CB_OnPlayerLoginInUnit,
	CB_OnPlayerRegisterInUnit,
	CB_OnPlayerChat,
	CB_OnPlayerCommandText,
	CB_OnPlayerClickTextDraw
};

native SetPlayerMoney(playerid, amount);

native SetScriptName(const name[]);
native GetScriptName(name[], len = sizeof(name));
native GetScriptNameEx(id, name[], len = sizeof(name));
native bool:IsThisScriptUnit();

native CallPublic(UnitCallback:publicid, scriptid, {_,Float}:...);
native BroadcastPublic(UnitCallback:publicid, expectedreturn, {_,Float}:...);

native GivePlayerModifier(playerid, Modifier:modifier);
native RemovePlayerModifier(playerid, Modifier:modifier);
native GetPlayerModifier(playerid, Modifier:modifier);

native CanPlayerTeleport(playerid, Float:x=0.0, Float:y=0.0, Float:z=0.0, Int=0);
native CanPlayerSpawnVehicle(playerid, model=0, Float:x=0.0, Float:y=0.0, Float:z=0.0, Int=0);

native Group:GetPlayerGroup(playerid);
native SetPlayerGroup(playerid, Group:group);
native SetPlayerGroupNamed(playerid, const group[]);

native IsReklama(const text[]);

//================| Time |======================================|
native Time_StampToDateTime(time, result[], maxsize = sizeof(result));
//================| Script |====================================|
native Script:Script_GetID(AMX:amx = AMX:0);
native AMX:Script_GetAmx(Script:id = INVALID_SCRIPT_ID);
//================| Util |======================================|
native Util_OptionPareser(const text[], const options[], &result, &rest, ...);
//================| Orig |======================================|
native Orig_GetPlayerMoney(playerid);
native Orig_GetPlayerAmmo(playerid);
native Orig_GetPlayerMenu(playerid);
native Orig_GetPlayerWeaponData(playerid, slot, &weapon, &ammo);
//================| Data |======================================|
native Data:Data_CreateString(const str[]);
native Data_Destroy(Data:data);

native Data_GetString(Data:data, str[], maxlen = sizeof(str));
//================| Data Set |==================================|
native DataSet_AddInt(DataSet:set, data);
native DataSet_AddInts(DataSet:set, &data, ...);
native DataSet_AddString(DataSet:set, const str[]);
//================| Commands |==================================|
native Cmd_AddGlobalDefault(Group:group);
native Cmd_RemoveGlobalDefault(Group:group);
native Cmd_AddGlobalDefaultNamed(const group[]);
native Cmd_RemoveGlobalDefaultNamed(const group[]);

native Command:Cmd_Create(const name[], const callback[], cmdid);
native Cmd_ReProcess(playerid, const cmd[], help = 0, sudo = 0);

native Command:Cmd_GetID(const name[]);
native Cmd_GetName(Command:command, name[], maxlen = sizeof(name));

native Cmd_SetHide(Command:commandid, set);
native Cmd_SetHideNamed(const name[], set);
native Cmd_GetHide(Command:commandid);
native Cmd_GetHideNamed(const name[]);

native Cmd_SetPlayer(Command:commandid, playerid, bool:set);
native Cmd_SetPlayerNamed(const commandname[], playerid, bool:set);
native Cmd_GetPlayer(Command:commandid, playerid);
native Cmd_GetPlayerNamed(const commandname[], playerid);
native Cmd_CheckPlayer(Command:commandid, playerid);
native Cmd_CheckPlayerNamed(const commandname[], playerid);

native Cmd_SetGroup(Command:commandid, Group:groupid, bool:set);
native Cmd_SetGroupNamed(const commandname[], Group:groupid, bool:set);
native Cmd_SetNamedGroup(Command:commandid, const groupname[], bool:set);
native Cmd_SetNamedGroupNamed(const commandname[], const groupname[], bool:set);

#pragma deprecated Should be used with macro only
native Cmd_SetDefaultGroup(Command:commandid, Group:groupid);
#pragma deprecated Should be used with macro only
native Cmd_SetDefaultGroupNamed(const commandname[], Group:groupid);
#pragma deprecated Should be used with macro only
native Cmd_SetDefaultNamedGroup(Command:commandid, const groupname[]);
#pragma deprecated Should be used with macro only
native Cmd_SetDefaultNamedGroupNamed(const commandname[], const groupname[]);

native Cmd_GetGroup(Command:commandid, Group:groupid);
native Cmd_GetNamedGroup(Command:commandid, const groupname[]);
native Cmd_GetGroupNamed(const commandname[], Group:groupid);
native Cmd_GetNamedGroupNamed(const commandname[], const groupname[]);

native Cmd_CheckGroup(Command:command, Group:group);
native Cmd_CheckNamedGroup(Command:command, const groupName[]);
native Cmd_CheckGroupNamed(const commandName[], Group:groupid);
native Cmd_CheckNamedGroupNamed(const commandName[], const groupname);

native Cmd_AddAlias(Command:command, const alias[]);
native Cmd_AddAliasNamed(const commandName[], const alias[]);
native Cmd_RemoveAlias(Command:command, const alias[]);

native Cmd_SetDisabled(Command:command, bool:set);
native Cmd_SetDisabledNamed(const commandname[], bool:set);
native Cmd_GetDisabled(Command:command);
native Cmd_GetDisabledName(const commandname[]);

native Script:Cmd_GetScriptID(Command:command);
native AMX:Cmd_GetScriptAmx(Command:command);

native SetPlayerCommandsDisabled(playerid, bool:set);
native ArePlayerCommandsDisabled(playerid);

#define Iterator@CommandID iterstart(-1)
native Command:Iter_Func@CommandID(start, const cmdName[]);

#define Iterator@CommandAlias iterstart(-1)
native Iter_Func@CommandAlias(start, Command:cmd, alias[], maxsize = sizeof(alias));

#define Iterator@Command iterstart(-1)
native Command:Iter_Func@Command(start);

#define Iterator@PlayerCommand iterstart(-1)
native Command:Iter_Func@PlayerCommand(start, pid);

#define Iterator@GroupCommand iterstart(-1)
native Command:Iter_Func@GroupCommand(start, Group:gid);

#define Iterator@AllCommand iterstart(-1)
native Command:Iter_Func@AllCommand(start);

#define Iterator@AllPlayerCommand iterstart(-1)
native Command:Iter_Func@AllPlayerCommand(start, pid);

#define Iterator@AllGroupCommand iterstart(-1)
native Command:Iter_Func@AllGroupCommand(start, Group:gid);
//================| RCommands |=================================|
/*
native RCmd_AddGlobalDefault(Group:group);
native RCmd_RemoveGlobalDefault(Group:group);
native RCmd_AddGlobalDefaultNamed(const group[]);
native RCmd_RemoveGlobalDefaultNamed(const group[]);

native RCommand:RCmd_Create(const name[], const callback[], cmdid);
native RCmd_ReProcess(uid, Group:group, const cmd[], help = 0, sudo = 0);

native RCommand:RCmd_GetID(const name[]);
native RCmd_GetName(RCommand:command, name[], maxlen = sizeof(name));

native RCmd_SetHide(RCommand:commandid, set);
native RCmd_SetHideNamed(const name[], set);
native RCmd_GetHide(RCommand:commandid);
native RCmd_GetHideNamed(const name[]);

native RCmd_CheckPlayer(RCommand:commandid, playerid);
native RCmd_CheckPlayerNamed(const commandname[], playerid);

native RCmd_SetGroup(RCommand:commandid, Group:groupid, bool:set);
native RCmd_SetGroupNamed(const commandname[], Group:groupid, bool:set);
native RCmd_SetNamedGroup(RCommand:commandid, const groupname[], bool:set);
native RCmd_SetNamedGroupNamed(const commandname[], const groupname[], bool:set);

native RCmd_SetDefaultGroup(RCommand:commandid, Group:groupid);
native RCmd_SetDefaultGroupNamed(const commandname[], Group:groupid);
native RCmd_SetDefaultNamedGroup(RCommand:commandid, const groupname[]);
native RCmd_SetDefaultNamedGroupNamed(const commandname[], const groupname[]);

native RCmd_GetGroup(RCommand:commandid, Group:groupid);
native RCmd_GetNamedGroup(RCommand:commandid, const groupname[]);
native RCmd_GetGroupNamed(const commandname[], Group:groupid);
native RCmd_GetNamedGroupNamed(const commandname[], const groupname[]);

native RCmd_CheckGroup(RCommand:commandid, Group:groupid);
native RCmd_CheckNamedGroup(RCommand:commandid, const groupname[]);
native RCmd_CheckGroupNamed(const commandname[], Group:groupid);
native RCmd_CheckNamedGroupNamed(const commandname[], const groupname);

native RCmd_AddAlias(RCommand:commandid, const alias[]);
native RCmd_AddAliasNamed(const command[], const alias[]);
native RCmd_RemoveAlias(const alias[]);

native RCmd_SetDisabled(Command:command, bool:set);
native RCmd_SetDisabledNamed(const commandname[], bool:set);
native RCmd_GetDisabled(Command:command);
native RCmd_GetDisabledName(const commandname[]);

#define Iterator@RCommand iterstart(-1)
native RCommand:Iter_Func@RCommand(start);

#define Iterator@PlayerRCommand iterstart(-1)
native RCommand:Iter_Func@PlayerRCommand(start, pid);

#define Iterator@GroupRCommand iterstart(-1)
native RCommand:Iter_Func@GroupRCommand(start, Group:gid);

#define Iterator@AllRCommand iterstart(-1)
native RCommand:Iter_Func@AllRCommand(start);

#define Iterator@AllPlayerRCommand iterstart(-1)
native RCommand:Iter_Func@AllPlayerRCommand(start, pid);

#define Iterator@AllGroupRCommand iterstart(-1)
native RCommand:Iter_Func@AllGroupRCommand(start, Group:gid);
*/
//================| Groups |====================================|
native Group:Group_Create(const name[], bool:locked = false, dbid = -1);
native Group_Destroy(Group:group);
native Group_DestroyNamed(const name[]);

native Group:Group_GetID(const name[]);
native Group_GetName(Group:group, name[], maxlen = sizeof(name));
native Group_GetDBID(Group:group);
native Group_SetDBID(Group:group, id);

native Group_SetPlayer(Group:group, playerid, bool:set);
native Group_SetPlayerNamed(const groupname[], playerid, bool:set);
native Group_GetPlayer(Group:group, playerid);
native Group_GetPlayerNamed(const groupname[], playerid);
native Group_CheckPlayer(Group:group, playerid);
native Group_CheckPlayerNamed(const groupname[], playerid);

native Group_SetGroup(Group:parent, Group:child, bool:set);
native Group_GetGroup(Group:child, Group:parent);
native Group_CheckGroup(Group:group, Group:othergroup);

native Group_SetColor(Group:group, color);
native Group_GetColor(Group:group);

#if defined Iterator@Group
	#undef Iterator@Group
#endif
#define Iterator@Group iterstart(-1)
native Group:Iter_Func@Group(start);

#define Iterator@PlayerGroup iterstart(-1)
native Group:Iter_Func@PlayerGroup(start, playerid);

#define Iterator@PlayerPermGroup iterstart(-1)
native Group:Iter_Func@PlayerPermGroup(start, playerid);

#define Iterator@GroupPlayer iterstart(-1)
native Iter_Func@GroupPlayer(start, Group:group);
//================| Permissions |====================================|
native Perm_AddGlobalDefault(Group:group);
native Perm_RemoveGlobalDefault(Group:group);
native Perm_AddGlobalDefaultNamed(const group[]);
native Perm_RemoveGlobalDefaultNamed(const group[]);

native Permission:Perm_GetID(const name[]);
native Perm_GetName(Permission:permission, name[], maxlen = sizeof(name));

native Permission:Perm_Default(const name[], Group:group);
native Permission:Perm_DefaultNamed(const name[], const groupname[]);

native Perm_SetGroup(Permission:perm, Group:group, bool:set);
native Perm_SetNamedGroup(Permission:perm, const groupname[], bool:set);
native Perm_SetGroupNamed(const permname[], Group:group, bool:set);
native Perm_SetNamedGroupNamed(const permname[], const groupname[], bool:set);

native Perm_GetGroup(Permission:perm, Group:group);
native Perm_GetNamedGroup(Permission:perm, const groupname[]);
native Perm_GetGroupNamed(const permname[], Group:group);
native Perm_GetNamedGroupNamed(const permname[], const groupname[]);

native Perm_CheckGroup(Permission:perm, Group:group);
native Perm_CheckNamedGroup(Permission:perm, const groupname[]);
native Perm_CheckGroupNamed(const permname[], Group:group);
native Perm_CheckNamedGroupNamed(const permname[], const groupname[]);

native Perm_CheckPlayer(Permission:perm, playerid);
native Perm_CheckPlayerNamed(const permname[], playerid);

#define Iterator@Permission iterstart(-1)
native Permission:Iter_Func@Permission(start);

#define Iterator@PlayerPermission iterstart(-1)
native Permission:Iter_Func@PlayerPermission(start, pid);

#define Iterator@GroupPermission iterstart(-1)
native Permission:Iter_Func@GroupPermission(start, Group:group);
//================| Dialogs |====================================|
native Dialog_Show(playerid, style, const title[], const caption[], const button1[] = "OK", const button2[] = "");
native Dialog_ShowCallback(playerid, const callback[], style, const title[], const caption[], const button1[], const button2[] = "", ...);
native Dialog_ShowCallbackData(playerid, const callback[], style, const title[], const caption[], const button1[], const button2[] = "", &DataSet:data);
native Dialog_ShowMessage(playerid, const title[], const caption[]);

native Dialog_IsShown(playerid);
native Dialog_Hide(playerid);

native Dialog_CreateIndexedCallback(playerid, const callback[], itemsperpage = 40);
native Dialog_SetIndexedHeader(playerid, const header[]);
native Dialog_AddIndexedListitem(playerid, index, const info[]);
native Dialog_ShowIndexed(playerid, const caption[], const button1[] = "Select", const button2[] = "Cancel", ...);
native Dialog_ShowIndexedData(playerid, const caption[], const button1[] = "Select", const button2[] = "Cancel", &DataSet:data);

native Dialog_ShowRegister(playerid, const callback[], minlength = 6, const caption[] = "Register", const info[] = "Zadajte svoje heslo:", const repeatinfo[] = "Zopakujte svoje heslo:", const button1[] = "Potvrdit", const button2[] = "Zrusit", ...);
native Dialog_ShowLogin(playerid, const callback[], const password[], const caption[] = "Login", const info[] = "Zadajte svoje heslo:", const button1[] = "Potvrdit", const button2[] = "Zrusit", ...);

native Dialog_ShowIndexedCallbackDB(playerid, const callback[], const caption[], DB2:db, const querry[], pagesize = 40, const header[]="", const button1[]="Select", const button2[]="Cancel", {DB2::DataType, QQPA}:...);
//================| TextDraw |===================================|
native TD_GetCount();
native TD_GetCountAll();

native TD_PrepareSpace(num);
native TD_FreeSpace(num);
//================| Crypto |=====================================|
enum CryptoData {
	CRYPTO_STRING,
	CRYPTO_BASE64,
	CRYPTO_ARRAY
}

enum HashType {
	CRYPTO_HASH_NONE,
	CRYPTO_HASH_MD5,
	CRYPTO_HASH_SHA1,
	CRYPTO_HASH_SHA224,
	CRYPTO_HASH_SHA256,
	CRYPTO_HASH_SHA384,
	CRYPTO_HASH_SHA512,
	CRYPTO_HASH_RIPEMD160
}

native Crypto_Random(result[], size, CryptoData:resultType = CRYPTO_BASE64, resultLen = sizeof(result));
native Crypto_Hash(const data[], result[], CryptoData:sourceType = CRYPTO_STRING, CryptoData:destType = CRYPTO_BASE64, srcLen = sizeof(data), resultLen = sizeof(result), HashType:type = CRYPTO_HASH_SHA256);
native Crypto_HMAC(const data[], const pass[], result[], CryptoData:sourceType = CRYPTO_STRING, CryptoData:keyType = CRYPTO_BASE64, CryptoData:destType = CRYPTO_BASE64, srcLen = sizeof(data), keyLen = sizeof(pass), resultLen = sizeof(result), HashType:type = CRYPTO_HASH_SHA256);
native Crypto_Encrypt(const iv[], const data[], const key[], result[], CryptoData:ivType = CRYPTO_STRING, CryptoData:dataType = CRYPTO_STRING, CryptoData:keyType = CRYPTO_BASE64, CryptoData:resultType = CRYPTO_BASE64, ivLen = sizeof(iv), dataLen = sizeof(data), keyLen = sizeof(key), resultLen = sizeof(result));
native Crypto_Decrypt(const iv[], const data[], const key[], result[], CryptoData:ivType = CRYPTO_STRING, CryptoData:dataType = CRYPTO_BASE64, CryptoData:keyType = CRYPTO_BASE64, CryptoData:resultType = CRYPTO_STRING, ivLen = sizeof(iv), dataLen = sizeof(data), keyLen = sizeof(key), resultLen = sizeof(result));

native Crypto_GenerateUnitKeys(result[], maxLen = sizeof(result));
native Crypto_SetUnitKeys(const keys[]);
native Crypto_UnitHMAC(const data[], result[], CryptoData:dataType = CRYPTO_STRING, CryptoData:resultType = CRYPTO_BASE64, dataLen = sizeof(data), maxLen = sizeof(result), HashType:type = CRYPTO_HASH_SHA256);
native Crypto_UnitEncrypt(const iv[], const data[], result[], CryptoData:ivType = CRYPTO_STRING, CryptoData:dataType = CRYPTO_STRING, CryptoData:resultType = CRYPTO_BASE64, ivLen = sizeof(iv), dataLen = sizeof(data), resultLen = sizeof(result));
native Crypto_UnitDecrypt(const iv[], const data[], result[], CryptoData:ivType = CRYPTO_STRING, CryptoData:dataType = CRYPTO_BASE64, CryptoData:resultType = CRYPTO_STRING, ivLen = sizeof(iv), dataLen = sizeof(data), resultLen = sizeof(result));
//================| SQLite |=====================================|
enum DB2::DataType {
	DB2::TYPE_NONE, // Not implemented
	DB2::TYPE_NULL,
	DB2::TYPE_INT,
	DB2::TYPE_UINT,
	DB2::TYPE_FLOAT,
	DB2::TYPE_STRING,
	DB2::TYPE_RAW_STRING, // Not implemented
	DB2::TYPE_IDENTIFIER, // Not implemented

	// Special types

	DB2::TYPE_PLAYER_NAME,
	DB2::TYPE_PLAYER_IP,
	DB2::TYPE_ARRAY,

	DB2::TYPE_MD5_HASH,
	DB2::TYPE_SHA1_HASH,
	DB2::TYPE_SHA256_HASH,
	DB2::TYPE_SHA512_HASH,
	DB2::TYPE_RIPEMD160_HASH,

	DB2::TYPE_MD5_ARRAY_HASH,
	DB2::TYPE_SHA1_ARRAY_HASH,
	DB2::TYPE_SHA256_ARRAY_HASH,
	DB2::TYPE_SHA512_ARRAY_HASH,
	DB2::TYPE_RIPEMD160_ARRAY_HASH,
	
	DB2::TYPE_DATA,
	
	DB2::TYPE_INVENTORY,
};


#define DB2_INT:          			DB2::TYPE_INT,QQPA:
#define DB2_INTEGER:      			DB2::TYPE_INT,QQPA:
#define DB2_UINT:         			DB2::TYPE_UINT,QQPA:
#define DB2_UINTEGER:     			DB2::TYPE_UINT,QQPA:
#define DB2_FLOAT:        			DB2::TYPE_FLOAT,QQPA:
#define DB2_STRING:       			DB2::TYPE_STRING,QQPA:
// #define DB2_RAW_STRING:   			DB2::TYPE_RAW_STRING,QQPA:
// #define DB2_IDENTIFIER:   			DB2::TYPE_IDENTIFIER,QQPA:

#define DB2_PLAYER_NAME:  			DB2::TYPE_PLAYER_NAME,QQPA:
#define DB2_PLAYER_IP:    			DB2::TYPE_PLAYER_IP,QQPA:
#define DB2_ARRAY(%0):    			DB2::TYPE_ARRAY,QQPA:%0,QQPA:

#define DB2_MD5_HASH:  				DB2::TYPE_MD5_HASH,QQPA:
#define DB2_SHA1_HASH:  			DB2::TYPE_SHA1_HASH,QQPA:
#define DB2_SHA256_HASH:  			DB2::TYPE_SHA256_HASH,QQPA:
#define DB2_SHA512_HASH:  			DB2::TYPE_SHA512_HASH,QQPA:
#define DB2_RIPEMD160_HASH: 		DB2::TYPE_RIPEMD160_HASH,QQPA:

#define DB2_MD5_AHASH(%0):  		DB2::TYPE_MD5_ARRAY_HASH,QQPA:%0,QQPA:
#define DB2_SHA1_AHASH(%0):  		DB2::TYPE_SHA1_ARRAY_HASH,QQPA:%0,QQPA:
#define DB2_SHA256_AHASH(%0):  		DB2::TYPE_SHA256_ARRAY_HASH,QQPA:%0,QQPA:
#define DB2_SHA512_AHASH(%0):  		DB2::TYPE_SHA512_ARRAY_HASH,QQPA:%0,QQPA:
#define DB2_RIPEMD160_AHASH(%0):  	DB2::TYPE_RIPEMD160_ARRAY_HASH,QQPA:%0,QQPA:
	
#define DB2_DATA:                   DB2::TYPE_DATA,QQPA:

#define DB2_INVENTORY:              DB2::TYPE_INVENTORY,QQPA:

#if defined UNIT_EASY_TYPE_TAGS
	#define INT:          			DB2::TYPE_INT,QQPA:
	#define INTEGER:      			DB2::TYPE_INT,QQPA:
	#define UINT:         			DB2::TYPE_UINT,QQPA:
	#define UINTEGER:     			DB2::TYPE_UINT,QQPA:
	#define FLOAT:        			DB2::TYPE_FLOAT,QQPA:
	#define STRING:       			DB2::TYPE_STRING,QQPA:
	// #define RAW_STRING:   			DB2::TYPE_RAW_STRING,QQPA:
	// #define IDENTIFIER:   			DB2::TYPE_IDENTIFIER,QQPA:

	#define PLAYER_NAME:  			DB2::TYPE_PLAYER_NAME,QQPA:
	#define PLAYER_IP:    			DB2::TYPE_PLAYER_IP,QQPA:
	#define ARRAY(%0):    			DB2::TYPE_ARRAY,QQPA:%0,QQPA:

	#define MD5_HASH:  				DB2::TYPE_MD5_HASH,QQPA:
	#define SHA1_HASH:  			DB2::TYPE_SHA1_HASH,QQPA:
	#define SHA256_HASH:  			DB2::TYPE_SHA256_HASH,QQPA:
	#define SHA512_HASH:  			DB2::TYPE_SHA512_HASH,QQPA:
	#define RIPEMD160_HASH: 		DB2::TYPE_RIPEMD160_HASH,QQPA:

	#define MD5_AHASH(%0):  		DB2::TYPE_MD5_ARRAY_HASH,QQPA:%0,QQPA:
	#define SHA1_AHASH(%0):  		DB2::TYPE_SHA1_ARRAY_HASH,QQPA:%0,QQPA:
	#define SHA256_AHASH(%0):  		DB2::TYPE_SHA256_ARRAY_HASH,QQPA:%0,QQPA:
	#define SHA512_AHASH(%0):  		DB2::TYPE_SHA512_ARRAY_HASH,QQPA:%0,QQPA:
	#define RIPEMD160_AHASH(%0):  	DB2::TYPE_RIPEMD160_ARRAY_HASH,QQPA:%0,QQPA:
	
	#define DATA:                   DB2::TYPE_DATA,QQPA:
	
	#define INVENTORY:              DB2::TYPE_INVENTORY,QQPA:
#endif

native DB2:db2_open(const name[]);
native db2_close(DB2:db);
native bool:db2_exists(DB2:db);
native db2_exec(DB2:db, const query[], {DB2::DataType, QQPA}:...);
native DB2Result:db2_query(DB2:db, const query[], {DB2::DataType, QQPA}:...);
native DB2AResult:db2_aquery(DB2:db, const query[], {DB2::DataType, QQPA}:...);
native DB2Result:db2_query_store(DB2:db, const query[], {DB2::DataType, QQPA}:...);
native DB2AResult:db2_aquery_store(DB2:db, const query[], {DB2::DataType, QQPA}:...);
native db2_free_result({DB2Result,DB2AResult}:result);
native db2_store_result({DB2Result,DB2AResult}:result);
native db2_result_exists({DB2Result,DB2AResult}:result);
native db2_is_null({DB2Result,DB2AResult}:result);
native db2_has_data({DB2Result,DB2AResult}:result);
native bool:db2_is_field_null({DB2Result,DB2AResult}:result, col);
native db2_get_field({DB2Result,DB2AResult}:result, col, data[], maxSize = sizeof(data));
native db2_get_field_int({DB2Result,DB2AResult}:result, col = 0);
native Float:db2_get_field_float({DB2Result,DB2AResult}:result, col = 0);
native db2_get_field_array({DB2Result,DB2AResult}:result, size, data[], col = 0);
native db2_next_row({DB2Result,DB2AResult}:result);
native db2_num_fields({DB2Result,DB2AResult}:result);
native db2_num_rows(DB2AResult:result);
native db2_set_row(DB2AResult:result, row);
native db2_get_row(DB2AResult:result);
native db2_changes(DB2:db);
native db2_total_changes(DB2:db);
native db2_last_insert_rowid(DB2:db);
native db2_query_int(DB2:db, const query[], {DB2::DataType, QQPA}:...);
native Float:db2_query_float(DB2:db, const query[], {DB2::DataType, QQPA}:...);
native db2_query_array(DB2:db, const query[], size, data[], {DB2::DataType, QQPA}:...);
native db2_insert(DB2:db, const query[], {DB2::DataType, QQPA}:...);
native db2_field_name({DB2Result,DB2AResult}:result, col, name[], maxsize = sizeof(name));

native DB2Prepared:db2_prepare_query(DB2:db, const query[]);
native db2_free_prepared_query(DB2Prepared:prepared);
native db2_exists_prepared_query(DB2Prepared:prepared);
native db2_bind_prepared_query(DB2Prepared:prepared);
native db2_exec_prepared_query(DB2Prepared:prepared, {DB2::DataType, QQPA}:...);
native DB2Result:db2_query_prepared_query(DB2Prepared:prepared, {DB2::DataType, QQPA}:...);
native DB2AResult:db2_aquery_prepared_query(DB2Prepared:prepared, {DB2::DataType, QQPA}:...);
native db2_reset_prepared_query(DB2Prepared:prepared);

stock bool:operator!(DB2:u_db2_db) return !db2_exists(u_db2_db);
stock bool:operator!(DB2Result:u_result) return !db2_result_exists(u_result);
stock bool:operator!(DB2AResult:u_result) return !db2_result_exists(u_result);
stock bool:operator!(DB2Prepared:u_prepared) return !db2_exists_prepared_query(u_prepared);

native db2_get_field_inv({DB2Result,DB2AResult}:result, Inventory:inv, col);
//================| Category |===================================|
native Category:Category_Create(const name[], bool:locked = true);
native Category_Destroy(Category:category);

native Category_Get(Category:category, rank);
native Category_Set(Category:category, rank, {_,Group}:value);
native Category_Insert(Category:category, rank, {_,Group}:value);
native Category_Push(Category:category, {_,Group}:value);
native Category_Erase(Category:category, rank);


#define Iterator@CategoryItem iterstart(-1)
native Iter_Func@CategoryItem(start, Category:category, &{_,Group}:value);

#define Iterator@RevCategoryItem iterstart(-1)
native Iter_Func@RevCategoryItem(start, Category:category, &{_,Group}:value);
//================| Inventory |==================================|
native Inventory:Inv_Create(max_weight = 0, max_size = 0, max_items = 0);
native Inv_Destroy(Inventory:id);
native Inv_Modify(Inventory:id, max_weight = 0, max_size = 0, max_items = 0);
native Inv_GetMax(Inventory:id, &max_weight, &max_size, &max_items);
native Inv_GetCur(Inventory:id, &weight, &size, &items);
native Inv_AddNewItem(Inventory:id, ItemType:item, count = 1);
native ItemType:Inv_GetItem(Inventory:id, slot);
native Inv_GetPoolSize(Inventory:id);
native Inv_TransferItem(Inventory:from, Inventory:to, slot, amount = 1);
native Inv_SaveContent(Inventory:id, data[], maxsize = sizeof(data));
native Inv_LoadContent(Inventory:id, const data[], size);

native ItemType:ItemType_Create(const name[], db_id, weight, size);
native ItemType_GetDBID(ItemType:id);
native ItemType_GetName(ItemType:id, name[], maxlen = sizeof(name));
native ItemType_GetWeight(ItemType:id);
native ItemType_GetSize(ItemType:id);
//================| Remote |=====================================|
enum REM_TYPE {
	REM_INT,
	REM_STRING,
	REM_CSTRING
}

native Remote_Call(Remote:id, {REM_TYPE,REM_DATA}:...);
native Remote_SetParams(Remote:id, const params[]);

#define global<%2>%3\32;%0(%1) UP_GL_1:UP_GL_2<%0,%2,%1>

#define UP_GL_1:UP_GL_2<%2:%0,%3,%1> @uY_%0(Remote:id); public @uY_%0(Remote:id) {Remote_SetParams(id,#%3);} %2:@uZ_%0(%1); public %2:@uZ_%0(%1)
#define UP_GL_2<%0,%3,%1> @uY_%0(Remote:id); public UP_GL_1:@uY_%0(Remote:id) {Remote_SetParams(id,#%3);} @uZ_%0(%1); public @uZ_%0(%1)

#define remote%2\32;%0(%1); public stock UP_REM_5:UP_REM_6<%0|||%1>
#define call_remote%0\32;%1(%2)    @uZ_%1(%2)

// Separate special Tag return functions from normaln functions
#define UP_REM_5:UP_REM_6<%2:%0|||%1> @uX_%0; stock %2:%0(%1) { return %2:UP_REM_E:UP_REM_CALL<%0>( %1); }
#define UP_REM_6<%0|||%1> @uX_%0; stock %0(%1) { return _:UP_REM_E:UP_REM_CALL<%0>( %1); }

// Separate case where no arguments are handed over
#define UP_REM_E:UP_REM_CALL<%0>(%1\32;) Remote_Call(Remote:@uX_%0)
#define UP_REM_CALL<%0>(%1) Remote_Call(Remote:@uX_%0,UP_REM(%1))

// Split at , to process individual arguments
#define UP_REM(%0) REM_TYPE:UP_REM_0:UP_REM_1:%0|||
#define UP_REM_0:UP_REM_1:%0,%1||| UP_REM_D1:UP_REM_D2:%0|||||,REM_TYPE:UP_REM_0:UP_REM_1:%1|||
#define UP_REM_1:%0||| UP_REM_D1:UP_REM_D2:%0|||||

// Remove default argument
#define UP_REM_D1:UP_REM_D2:%0=%1||||| UP_REM_2:UP_REM_3:UP_REM_4:%0||||
#define UP_REM_D2:%0||||| UP_REM_2:UP_REM_3:UP_REM_4:%0||||

// Parse types and pass them along
#define UP_REM_2:UP_REM_3:UP_REM_4:%1const%0[]|||| REM_CSTRING,REM_DATA:%0
#define UP_REM_3:UP_REM_4:%0[]|||| REM_STRING,REM_DATA:%0
#define UP_REM_4:%0|||| REM_INT,REM_DATA:%0
//================| Publics |====================================|
forward OnAmxLoad(id);
forward OnAmxUnload(id);

forward OnPlayerTryWarp(playerid, Float:x, Float:y, Float:z, Int);
forward OnPlayerWarp   (playerid, Float:x, Float:y, Float:z, Int);

forward OnPlayerTrySpawnVehicle(playerid, model, Float:x, Float:y, Float:z, Int);

forward OnPlayerRegisterInUnit(playerid, uid);
forward OnPlayerLoginInUnit(playerid, Group:group, uid);

forward OnPlayerChat(playerid, const text[]);

forward e_COMMAND_ERRORS:OnCommandReceived(playerid, const cmdtext[], help, sudo, e_COMMAND_ERRORS:success);
forward e_COMMAND_ERRORS:OnCommandRecognized(playerid, Command:commandid, const cmdtext[], help, sudo, e_COMMAND_ERRORS:success);
forward e_COMMAND_ERRORS:OnCommandPerformed(playerid, const cmdtext[], help, sudo, e_COMMAND_ERRORS:success);
/*
forward e_COMMAND_ERRORS:OnRCommandReceived(uid, Group:group, const cmdtext[], help, sudo, e_COMMAND_ERRORS:success);
forward e_COMMAND_ERRORS:OnRCommandRecognized(uid, Group:group, RCommand:commandid, const cmdtext[], help, sudo, e_COMMAND_ERRORS:success);
forward e_COMMAND_ERRORS:OnRCommandPerformed(uid, Group:group, const cmdtext[], help, sudo, e_COMMAND_ERRORS:success);
*/

forward OnDialogClosed(playerid);

forward OnPlayerModifierAdded(playerid, Modifier:modifier);
forward OnPlayerModifierRemoved(playerid, Modifier:modifier);

forward OnPlayerChangeVW(playerid, newWorld, oldWorld);

// Unit Areas Publics
forward OnPlayerLeaveUTempArea(playerid);
forward OnPlayerEnterUTempArea(playerid);

forward OnUnitPermsLoaded();

#if defined UNIT_ANTICHEAT
	enum Cheats {
        None,
        WeaponHack,
        AmmoHack,
        CarHack,
        CarHackSlow,
        CarHackFake,
        CarHackData,
        MenuFlood,
        MenuSpoof
    }

	forward OnCheatDetected(playerid, Cheats:cheat, extra1, extra2);
#endif
//===================================================================================================|
#if !defined UNIT_NO_EXTENDED
	#if !defined UNIT_CMD_HELP_CAP
	    #define UNIT_CMD_HELP_CAP "{FF0000}Command"
	#endif
	#define HELP(%0) if(_:UP_T1:UP_T2:%0|||
	#define UP_T1:UP_T2:%0,%1||| help || (%1)) return Dialog_ShowMessage(playerid, UNIT_CMD_HELP_CAP, _:%0)
	#define UP_T2:%0||| help) return Dialog_ShowMessage(playerid, UNIT_CMD_HELP_CAP, _:%0)
	
	#define RHELP(%0) if(_:UP_T3:UP_T4:%0|||
	#define UP_T3:UP_T4:%0,%1||| help || (%1)) return SendSecureRemoteReply(_:%0)
	#define UP_T4:%0||| help) return SendSecureRemoteReply(_:%0)
	
	#define function%0(%1)      forward %0(%1); public %0(%1)
	#define private%0\32;%1(%2) forward @uP_%1(%2); public @uP_%1(%2)
	#define call_private%0\32;%1(%2)    @uP_%1(%2)
#endif
//===================================================================================================|
#if !defined UNIT_NO_API

stock IsPlayerAbovePlayer(playerid, otherid) {
	if(playerid == otherid) return 1;
	return (Group_CheckPlayer(GetPlayerGroup(otherid), playerid) && GetPlayerGroup(playerid) != GetPlayerGroup(otherid));
}

stock IsPlayerAboveGroup(playerid, Group:group) {
	return (Group_CheckPlayer(group, playerid) && GetPlayerGroup(playerid) != group);
}

stock IsGroupAbovePlayer(Group:group, otherid) {
	return (Group_CheckGroup(group, GetPlayerGroup(otherid)) && group != GetPlayerGroup(otherid));
}

stock IsGroupAboveGroup(Group:group, Group:otherGroup) {
	return (Group_CheckGroup(group, otherGroup) && group != otherGroup);
}

remote bool:IsPlayerAbovePlayerName(playerid, const othername[]);
remote bool:IsPlayerAbovePlayerIP(playerid, const otherip[]);

stock GetGroupName(playerid)
{
	new name[33];
	Group_GetName(GetPlayerGroup(playerid), name);
    return name;
}

stock GetGroupNameByID(Group:id)
{
    new name[33];
	Group_GetName(id, name);
	return name;
}

remote PutPlayerInUnitJail(playerid, time);
remote ReleasePlayerFromUnitJail(playerid);
remote IsPlayerInUnitJail(playerid);

remote MutePlayer(playerid, time = 0);
remote UnmutePlayer(playerid);
remote IsPlayerMuted(playerid);

remote bool:IsPlayerFrozen(playerid);

remote UnitBan(playerid, const admin[] = "Neznamy", const duvod[] = "Neznamy", const extra[] = "");
remote UnitTimeBan(playerid, minut, hodin, dni, const admin[] = "Neznamy", const duvod[] = "Neznamy", const extra[] = "");

remote GivePlayerGroup(playerid, Group:group, time = 0);

remote VarPlayer(playerid);
remote PardonPlayer(playerid);
remote GetPlayerVars(playerid);

remote bool:IsLoggedInInUnit(playerid);
remote bool:IsRegisteredInUnit(playerid);
remote ShowUnitLogin(playerid);
remote ShowUnitRegister(playerid);

// IsPlayerInCombat(playerid) - funkcia vr�ti 1 ak hr�� hitol, alebo dostal hit po�as posledn�ch 30 sek�nd.
remote IsPlayerInCombat(playerid);


stock bool:UnitIsReklama(playerid, const text[]) {
	if(IsReklama(text)) {
		new h_str[128], h_name[MAX_PLAYER_NAME+1];
		GetPlayerName(playerid, h_name, sizeof(h_name));
		format(h_str, sizeof(h_str), "%s %s bol zabanovany za reklamu.", GetGroupName(playerid),h_name);
		SendClientMessageToAll(0xF60000AA,h_str);
		printf("REKLAMA (%s): %s", h_name, text);
		UnitBan(playerid, "unit", "Reklama");
		return true;
	}
	return false;
}





stock SendClientMessageToAdmins(color, const message[]) {
	new Permission:perm = Perm_GetID("Admin");
	for(new i=0,max_pool=GetPlayerPoolSize();i<max_pool;++i) {
	    if(Perm_CheckPlayer(perm, i)) SendClientMessage(i, color, message);
	}
	return 1;
}

stock SendClientMessageToPerm(Permission:permission, color, const message[]) {
    for(new i=0,idmax=GetPlayerPoolSize();i<idmax;++i) {
	    if(Perm_CheckPlayer(permission, i)) SendClientMessage(i, color, message);
	}
	return 1;
}





remote GetUserID(playerid);
remote GetUserIDNamed(const name[]);
remote Group:GetPlayerGroupDB(playerid);
remote Group:GetPlayerGroupName(const name[]);
remote Group:GetPlayerGroupIP(const ip[]);
remote Group:GetPlayerGroupUID(uid);


remote SendInfoMessage(const message[]);

remote Error:AddDBToUnit(DB2:db, const name[]);

// Unit Areas remotes
// IsPlayerInUnitTempArea(playerid) - vrati 1, ak sa hrac nachadza v arey (ak sa robi nejaky event napr.), ak sa nenachadza vrati 0
remote IsPlayerInUnitTempArea(playerid);

// IsUnitTempAreaCreated vrati 1 ak nejaka unit area je vytvorena (docastna area cez prikaz /areas) ak nie je vrati 0
remote IsUnitTempAreaCreated();
#endif

#if defined UNIT_ZCMD
	#define CMD: HCMD:
	#define COMMAND: HCMD:
#endif

//#define HCMD:%0(%1) forward @uC_%0(playerid, params[], help, sudo); public @uC_%0(%1)

native _dep_cmd_SDNG(Command:cmd, const group[]) = Cmd_SetDefaultNamedGroup;

// Basic Parse
#define HCMD%0:%1(%2) hm1:hm2(%1,%0) @uC_%1(playerid,params[],help,sudo); public @uC_%1(%2)

// Check if [] exists
#define hm1:hm2(%0,)
#define hm2(%0,[%1]) @uG_%0(Command:id); public hm1:@uG_%0(Command:id) { !(hm8:hm5:hm3:hm4(%1)); }

// Split |, $, and parse default groups
#define hm8:hm5:hm3:hm4(%1$%2) hm5:hm3:hm4(%1)); !(Cmd_SetHide(id, _:hm9:hm10(%2))
#define hm5:hm3:hm4(%1|%2) hm3:hm4(%1)); !(hm6:hm7(%2)
#define hm3:hm4(%1,%2) _dep_cmd_SDNG(id, #%1)); !(hm3:hm4(%2)
#define hm4(%1) _dep_cmd_SDNG(id, #%1)

// Split aliases
#define hm6:hm7(%1,%2) Cmd_AddAlias(id, #%1)); !(hm6:hm7(%2)
#define hm7(%1) Cmd_AddAlias(id, #%1)

// Add COMMAND_HIDE_
#define hm9:hm10(%1,%2) COMMAND_HIDE_%1|_:hm9:hm10(%2)
#define hm10(%1) COMMAND_HIDE_%1
/*
#define RCMD%0:%1(%2) rm1:rm2(%1,%0) @uR_%1(uid,params[],Group:group,help,sudo); public @uR_%1(%2)
#define rm1:rm2(%0,)
#define rm2(%0,[%1]) @uW_%0(RCommand:id); public rm1:@uW_%0(RCommand:id) { !(rm5:rm3:rm4(%1)); }
#define rm5:rm3:rm4(%1|%2) rm3:rm4(%1)); !(rm6:rm7(%2)
#define rm3:rm4(%1,%2) RCmd_SetDefaultNamedGroup(id, #%1)); !(rm3:rm4(%2)
#define rm4(%1) RCmd_SetDefaultNamedGroup(id, #%1)
#define rm6:rm7(%1,%2) RCmd_AddAlias(id, #%1)); !(rm6:rm7(%2)
#define rm7(%1) RCmd_AddAlias(id, #%1)
*/
//===========================| Hooky |=========================================
//=============================================================================|
#if !defined isnull
	#define isnull(%1) \
				((!(%1[0])) || (((%1[0]) == '\1') && (!(%1[1]))))
#endif
































//==============| Tests |======================================================|
#if defined UNIT_TESTS
native Test_RunAll();

#define TEST(%0) @uT_%0(); public @uT_%0()
#define ASSERT(%0,%1); if(!(%0)) { printf("    FAILED: %s", %1); return 2; }
#endif














