/*
 *   United Adminscript Tests
 *
 *  Autor: 				xhunterx
 *  Date created:       2016/11/13
 *  Credits:
 *
 *  Description:        Toto je zbierka testov navrhnuta na testovanie funkcii
 *                      unit pluginu a verejnych funkcii unit adminscriptu.
 */


#define     USE_NPC
#define     DELAYED_TEST
//#define   IMEDIATE_TEST














#define UNIT_TESTS

#include <a_samp>
#include <unitp.inc>
#include "gitversion.pwn"

new numNPCs;
new NPCs[10] = {-1, -1, ...};

new numPlayers = 0;
new Players[10];

public OnFilterScriptInit() {

	#if defined USE_NPC
	ConnectNPC("TestPlayer1", "train_lv");
	ConnectNPC("TestPlayer2", "train_ls");
	ConnectNPC("TestPlayer3", "train_sf");
	#endif
	
	#if defined DELAYED_TEST
	SetTimer(#RunTests, 3000, false);
	#endif
	
	#if defined IMEDIATE_TEST
	RunTests();
	#endif
	return 1;
}

function RunTests() {
    print("\n   +------------------------------------+");
	print(  "   |  *** United Adminscript Tests ***  |");
	print(  "   +------------------------------------+\n");

 	print(  "   +------------------------------------+");
	print(  "   |  Autor:                 xhunterx   |");
	print(  "   |  SA-MP Version 0.3.7 R2 Linux/Win  |");
	printf( "   |  Git SHA1 Version:      %s    |", GIT_SHA1_SHORT);
	print(  "   +------------------------------------+\n");

    Test_RunAll();
}

public OnPlayerConnect(playerid) {
	if(IsPlayerNPC(playerid)) NPCs[numNPCs++] = playerid;
	else Players[numPlayers++] = playerid;
	return 1;
}

#include "Group/basic.pwn"
#include "Permission/basic.pwn"
#include "Modifier/basic.pwn"

TEST(NPC_Connect) {
	new count = 0;
	for(new i=0; i<MAX_PLAYERS; ++i) {
	    if(IsPlayerConnected(i) && IsPlayerNPC(i)) ++count;
	}
	ASSERT(count >= 3, "Could not connect NPCs. Please check your server config.");
	return 1;
}



















