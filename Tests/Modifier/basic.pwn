/*
 *   United Adminscript Tests
 *
 *  Autor: 				xhunterx
 *  Date created:       2016/11/13
 *  Credits:
 *
 *  Description:        Toto je zbierka testov navrhnuta na testovanie funkcii
 *                      unit pluginu a verejnych funkcii unit adminscriptu.
 *
 *  Instructions:       Ak pridate testy do tohoto suboru, alebo inak prispejete,
 *	                    dopiste svoj nick do credits.
 */





TEST(Mod_Basic) {
	new playerid = NPCs[0];
    ASSERT(GetPlayerModifier(playerid, E_Mod_GodMode) == 0, "Chyba poctu modifierov 0.");
	GivePlayerModifier(playerid, E_Mod_GodMode);
	ASSERT(GetPlayerModifier(playerid, E_Mod_GodMode) == 1, "Chyba poctu modifierov 1.");
	RemovePlayerModifier(playerid, E_Mod_GodMode);
	ASSERT(GetPlayerModifier(playerid, E_Mod_GodMode) == 0, "Chyba poctu modifierov 2.");

	for(new i=0; i<20; ++i) {
	    GivePlayerModifier(playerid, E_Mod_GodMode);
		ASSERT(GetPlayerModifier(playerid, E_Mod_GodMode) == i+1, "Chyba poctu modifierov 3.");
	}
	for(new i=20; i<0; --i) {
	    RemovePlayerModifier(playerid, E_Mod_GodMode);
		ASSERT(GetPlayerModifier(playerid, E_Mod_GodMode) == i-1, "Chyba poctu modifierov 3.");
	}
	return 1;
}

TEST(Mod_Random) {
    new mods = 0, playerid = NPCs[0];
	ASSERT(GetPlayerModifier(playerid, E_Mod_GodMode) == 0, "Chyba poctu modifierov 0.");
	for(new i=0; i<1000; ++i) {
		switch(random(2)) {
		    case 0: {
		        ++mods;
		        GivePlayerModifier(playerid, E_Mod_GodMode);
		        ASSERT(GetPlayerModifier(playerid, E_Mod_GodMode) == mods, "Chyba poctu modifierov case 0.");
		    }
		    case 1: {
		        if(mods) --mods;
		        RemovePlayerModifier(playerid, E_Mod_GodMode);
		        ASSERT(GetPlayerModifier(playerid, E_Mod_GodMode) == mods, "Chyba poctu modifierov case 1.");
		    }
		}
	}
	while(mods--) {
	    RemovePlayerModifier(playerid, E_Mod_GodMode);
	    ASSERT(GetPlayerModifier(playerid, E_Mod_GodMode) == mods, "Chyba poctu modifierov case 2.");
	}
	return 1;
}





























