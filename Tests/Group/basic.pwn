/*
 *   United Adminscript Tests
 *
 *  Autor: 				xhunterx
 *  Date created:       2016/11/13
 *  Credits:
 *
 *  Description:        Toto je zbierka testov navrhnuta na testovanie funkcii
 *                      unit pluginu a verejnych funkcii unit adminscriptu.
 *
 *  Instructions:       Ak pridate testy do tohoto suboru, alebo inak prispejete,
 *	                    dopiste svoj nick do credits.
 */


















TEST(Group_Basic) {
	new Group:g = Group_Create("TestGroup");
	ASSERT(g != INVALID_GROUP_ID, "Can't create group.");
	ASSERT(Group_GetID("TestGroup") == g, "Can't match group.");
	Group_Destroy(g);
	return 1;
}

TEST(Group_WierdName) {
	new Group:g;

	new const name1[] = "@#$@#$%^&*(13 542\\avfoque vqFAVOR";

	g = Group_Create(name1);
	ASSERT(g != INVALID_GROUP_ID, "Could not create group.");
	ASSERT(Group_GetID(name1) == g, "Could not match group.");
	Group_Destroy(g);
	return 1;
}
















