/*
 *   United Adminscript Tests
 *
 *  Autor: 				xhunterx
 *  Date created:       2016/11/13
 *  Credits:
 *
 *  Description:        Toto je zbierka testov navrhnuta na testovanie funkcii
 *                      unit pluginu a verejnych funkcii unit adminscriptu.
 *
 *  Instructions:       Ak pridate testy do tohoto suboru, alebo inak prispejete,
 *	                    dopiste svoj nick do credits.
 */











TEST(Perm_Basic) {
	new Group:g = Group_Create("TestGroup");
	ASSERT(g != INVALID_GROUP_ID, "Can't create Group.");

	new Permission:perm;
	perm = Perm_DefaultNamed("TestPerm", "TestGroup");
	ASSERT(perm != INVALID_PERMISSION_ID, "Can't create perm.");
	ASSERT(Perm_GetGroup(perm, g), "Perm bad value on Get.");

	Group_Destroy(g);
	return 1;
}

























