#if defined __unit_sscanf__included__
    #endinput
#else
    #define __unit__sscanf__included__
#endif

#include <unit>
#include <sscanf2>

new WeaponName[][28] =
{
	{"Unarmed"},
	{"Brass"},
	{"Golf club"},
	{"Nite Stick"},
	{"Knife"},
	{"Baseball Bat"},
	{"Shovel"},
	{"Pool Cue"},
	{"Katana"},
	{"Chainsaw"},
	{"Purple Dildo"},
	{"Small White Vibrator"},
	{"Large White Vibrator"},
	{"Silver Vibrator"},
	{"Flowers"},
	{"Cane"},
	{"Grenade"},
	{"Teargas"},
	{"Molotov Cocktail"},
	{" "},
	{" "},
	{" "},
	{"9mm"},
	{"Silenced 9 mm"},
	{"Desert Eagle"},
	{"Shotgun"},
	{"Sawnoff Shotgun"},
	{"Combat Shotgun"},
	{"Micro Uzi"},
	{"SMG"},
	{"AK47"},
	{"M4"},
	{"Tec9"},
	{"Country Rifle"},
	{"Sniper Rifle"},
	{"RPG"},
	{"HS Rocket Launcher"},
	{"Flamethrower"},
	{"Minigun"},
	{"C4"},
	{"Detonator"},
	{"Spray"},
	{"Fire Extinguisher"},
	{"Camera"},
	{"Nightvision Goggles"},
	{"Thermal Goggles"},
	{"Parachute"},
	{" "}
};

#define GET_VEH_NAME(%0) VehicleName[((%0) - 400)]
new VehicleName[212][] =
{
	{"Landstalker"},
	{"Bravura"},
	{"Buffalo"},
	{"Linerunner"},
	{"Perrenial"},
	{"Sentinel"},
	{"Dumper"},
	{"Firetruck"},
	{"Trashmaster"},
	{"Stretch"},
	{"Manana"},
	{"Infernus"},
	{"Voodoo"},
	{"Pony"},
	{"Mule"},
	{"Cheetah"},
	{"Ambulance"},
	{"Leviathan"},
	{"Moonbeam"},
	{"Esperanto"},
	{"Taxi"},
	{"Washington"},
	{"Bobcat"},
	{"Mr Whoopee"},
	{"BF Injection"},
	{"Hunter"},
	{"Premier"},
	{"Enforcer"},
	{"Securicar"},
	{"Banshee"},
	{"Predator"},
	{"Bus"},
	{"Rhino"},
	{"Barracks"},
	{"Hotknife"},
	{"Trailer 1"},
	{"Previon"},
	{"Coach"},
	{"Cabbie"},
	{"Stallion"},
	{"Rumpo"},
	{"RC Bandit"},
	{"Romero"},
	{"Packer"},
	{"Monster"},
	{"Admiral"},
	{"Squalo"},
	{"Seasparrow"},
	{"Pizzaboy"},
	{"Tram"},
	{"Trailer 2"},
	{"Turismo"},
	{"Speeder"},
	{"Reefer"},
	{"Tropic"},
	{"Flatbed"},
	{"Yankee"},
	{"Caddy"},
	{"Solair"},
	{"Berkley's RC Van"},
	{"Skimmer"},
	{"PCJ-600"},
	{"Faggio"},
	{"Freeway"},
	{"RC Baron"},
	{"RC Raider"},
	{"Glendale"},
	{"Oceanic"},
	{"Sanchez"},
	{"Sparrow"},
	{"Patriot"},
	{"Quad"},
	{"Coastguard"},
	{"Dinghy"},
	{"Hermes"},
	{"Sabre"},
	{"Rustler"},
	{"ZR-350"},
	{"Walton"},
	{"Regina"},
	{"Comet"},
	{"BMX"},
	{"Burrito"},
	{"Camper"},
	{"Marquis"},
	{"Baggage"},
	{"Dozer"},
	{"Maverick"},
	{"News Chopper"},
	{"Rancher"},
	{"FBI Rancher"},
	{"Virgo"},
	{"Greenwood"},
	{"Jetmax"},
	{"Hotring"},
	{"Sandking"},
	{"Blista Compact"},
	{"Police Maverick"},
	{"Boxville"},
	{"Benson"},
	{"Mesa"},
	{"RC Goblin"},
	{"Hotring Racer A"},
	{"Hotring Racer B"},
	{"Bloodring Banger"},
	{"Rancher"},
	{"Super GT"},
	{"Elegant"},
	{"Journey"},
	{"Bike"},
	{"Mountain Bike"},
	{"Beagle"},
	{"Cropdust"},
	{"Stunt"},
	{"Tanker"},
	{"Roadtrain"},
	{"Nebula"},
	{"Majestic"},
	{"Buccaneer"},
	{"Shamal"},
	{"Hydra"},
	{"FCR-900"},
	{"NRG-500"},
	{"HPV1000"},
	{"Cement Truck"},
	{"Tow Truck"},
	{"Fortune"},
	{"Cadrona"},
	{"FBI Truck"},
	{"Willard"},
	{"Forklift"},
	{"Tractor"},
	{"Combine"},
	{"Feltzer"},
	{"Remington"},
	{"Slamvan"},
	{"Blade"},
	{"Freight"},
	{"Streak"},
	{"Vortex"},
	{"Vincent"},
	{"Bullet"},
	{"Clover"},
	{"Sadler"},
	{"Firetruck LA"},
	{"Hustler"},
	{"Intruder"},
	{"Primo"},
	{"Cargobob"},
	{"Tampa"},
	{"Sunrise"},
	{"Merit"},
	{"Utility"},
	{"Nevada"},
	{"Yosemite"},
	{"Windsor"},
	{"Monster A"},
	{"Monster B"},
	{"Uranus"},
	{"Jester"},
	{"Sultan"},
	{"Stratum"},
	{"Elegy"},
	{"Raindance"},
	{"RC Tiger"},
	{"Flash"},
	{"Tahoma"},
	{"Savanna"},
	{"Bandito"},
	{"Freight Flat"},
	{"Streak Carriage"},
	{"Kart"},
	{"Mower"},
	{"Duneride"},
	{"Sweeper"},
	{"Broadway"},
	{"Tornado"},
	{"AT-400"},
	{"DFT-30"},
	{"Huntley"},
	{"Stafford"},
	{"BF-400"},
	{"Newsvan"},
	{"Tug"},
	{"Trailer 3"},
	{"Emperor"},
	{"Wayfarer"},
	{"Euros"},
	{"Hotdog"},
	{"Club"},
	{"Freight Carriage"},
	{"Trailer 3"},
	{"Andromada"},
	{"Dodo"},
	{"RC Cam"},
	{"Launch"},
	{"Police Car (LSPD)"},
	{"Police Car (SFPD)"},
	{"Police Car (LVPD)"},
	{"Police Ranger"},
	{"Picador"},
	{"S.W.A.T. Van"},
	{"Alpha"},
	{"Phoenix"},
	{"Glendale"},
	{"Sadler"},
	{"Luggage Trailer A"},
	{"Luggage Trailer B"},
	{"Stair Trailer"},
	{"Boxville"},
	{"Farm Plow"},
	{"Utility Trailer"}
};

static stock _equal(const s1[], const s2[], bool:caps=false) {
    if(strlen(s1) != strlen(s2)) return false;
    if(strcmp(s1,s2,caps) == 0) return true;
    return false;
}

SSCANF:group(string[]) {
	return _:Group_GetID(string);
}

SSCANF:color(string[]) {
	if(1<=strval(string)<=8) return strval(string);
	
	if(_equal(string,"admin",true))  	return 1;
	if(_equal(string,"modra",true))  	return 2;
	if(_equal(string,"cervena",true))  	return 3;
	if(_equal(string,"oranzova",true))  	return 4;
	if(_equal(string,"bila",true))  		return 5;
	if(_equal(string,"cerna",true))  	return 6;
	if(_equal(string,"zelena",true))  	return 7;
	if(_equal(string,"zluta",true))  	return 8;
	return -1;
}

SSCANF:fight_style(string[]) {
	if(_equal(string,"normalne",true))  	return FIGHT_STYLE_NORMAL;
	if(_equal(string,"box",true))  		return FIGHT_STYLE_BOXING;
	if(_equal(string,"kung-fu",true))  	return FIGHT_STYLE_KUNGFU;
	if(_equal(string,"kungfu",true))  	return FIGHT_STYLE_KUNGFU;
	if(_equal(string,"kung fu",true))  	return FIGHT_STYLE_KUNGFU;
	if(_equal(string,"knee-head",true))	return FIGHT_STYLE_KNEEHEAD;
	if(_equal(string,"kneehead",true))	return FIGHT_STYLE_KNEEHEAD;
	if(_equal(string,"knee head",true))	return FIGHT_STYLE_KNEEHEAD;
	if(_equal(string,"grab-kick",true))  return FIGHT_STYLE_GRABKICK;
	if(_equal(string,"grabkick",true))	return FIGHT_STYLE_GRABKICK;
	if(_equal(string,"grab kick",true))	return FIGHT_STYLE_GRABKICK;
	if(_equal(string,"elbow",true))  	return FIGHT_STYLE_ELBOW;
	return -1;
}

SSCANF:find_weapon(string[]) {
	// This function is VERY basic, needs VASTLY improving to detect variations.
	if ('0' <= string[0] <= '9') {
		new ret = strval(string);
		if (0 <= ret <= 18 || 22 <= ret <= 46) {
			return ret;
		}
	}
	else {
        for(new i=0;i<48;i++) {
        	if(i == 19 || i == 20 ||i == 21) continue;
			if(strfind(WeaponName[i],string,true)!=-1)	return i;
		}
	}
	return -1;
}

SSCANF:find_vehicle(string[]) {
	// This function is VERY basic, needs VASTLY improving to detect variations.
	if ('0' <= string[0] <= '9') {
		new ret = strval(string);
		if (400 <= ret <= 611) {
			return ret;
		}
	}
	else {
	    for(new i=0;i<212;i++) {
			if(strfind(VehicleName[i],string,true) != -1) {
				return 400+i;
			}
		}
	}
	return -1;
}
